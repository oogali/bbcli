from setuptools import setup

setup(
    name='bb',
    version='0.3',
    py_modules=['bb', 'bb.cli', 'bb.client'],
    install_requires=[
        'docopt',
        'pybitbucket'
    ],
    entry_points='''
        [console_scripts]
        bb=bb.cli:main
    '''
)
