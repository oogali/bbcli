from __future__ import print_function
import os
import pybitbucket
import pybitbucket.auth
import pybitbucket.bitbucket
import pybitbucket.repository
from collections import namedtuple
from ConfigParser import SafeConfigParser


DEFAULT_CONFIG_FILE = '~/.bb.cfg'


class BitbucketClient(object):
    Repo = namedtuple('Repo', 'owner name description language')

    def __init__(self):
        self.username = None
        self.password = None
        self.email = None

    def load_config(self, config_file=DEFAULT_CONFIG_FILE):
        config_file = os.path.expanduser(config_file)
        if not os.path.exists(config_file):
            raise Exception('Configuration file does not exist')

        cfg = SafeConfigParser()
        cfg.read(config_file)

        try:
            self.username = cfg.get('auth', 'username')
            self.password = cfg.get('auth', 'password')
            self.email = cfg.get('auth', 'email')
        except:
            raise Exception('Username, password, and e-mail address must be specified in configuration file')

    @property
    def auth(self):
        if not self.username or not self.password or not self.email:
            self.load_config()

        return pybitbucket.auth.BasicAuthenticator(self.username, self.password, self.email)

    @property
    def client(self):
        return pybitbucket.bitbucket.Client(self.auth)

    @property
    def bitbucket(self):
        return pybitbucket.bitbucket.Bitbucket(client=self.client)

    def repos(self, owner=None):
        our_repos = []
        repos = self.bitbucket.repositoriesByOwnerAndRole(owner=owner,
                                                          role=pybitbucket.repository.RepositoryRole.MEMBER)
        for repo in repos:
            username = repo.owner['username'] if isinstance(repo.owner, dict) else repo.owner.username
            r = BitbucketClient.Repo(owner=username,
                                 name=repo.name,
                                 description=repo.description,
                                 language=repo.language)

            our_repos.append(r)

        return our_repos

    def create_repo(self, name, description=None, language=None, public=False, issues=True, wiki=True):
        if '/' not in name:
            owner = None
            name = name
        else:
            owner, name = name.split('/', 2)

        repo = pybitbucket.repository.Repository.create(name,
                                                        pybitbucket.repository.RepositoryForkPolicy.NO_PUBLIC_FORKS,
                                                        is_private=(not public),
                                                        owner=owner,
                                                        description=description,
                                                        scm=pybitbucket.repository.RepositoryType.GIT,
                                                        has_issues=issues,
                                                        has_wiki=wiki,
                                                        language=language,
                                                        client=self.client)

        return repo