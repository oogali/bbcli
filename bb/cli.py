"""bbcli

Usage:
    bbcli (-h | --help)
    bbcli [--debug] [--config=<filename>] <command> ...
    bbcli configure [--user=<user>] [--password=<password>] [--email=<email>]
    bbcli repos [--by-owner=<owner>]
    bbcli create repo [--language=<language>] [--public] <repo>

Options:
    -h --help                   Display this help message
    --debug                     Enable logging of debugging messages
    --config=<filename>         Path to bbcli configuration file [default: ~/.bb.cfg]

Commands:
    configure:
        --user=<user>           Your Bitbucket username
        --password=<password>   Your Bitbucket password
        --email=<email>         Your Atlassian ID e-mail address

    repos:
        --by-owner=<owner>      List all repositories owned by a particular username

    create repo:
        --language=<language>   Language of this repository (e.g. ruby, python, perl, etc)
        --public                Make this new repository public

"""

from __future__ import print_function
import os
import sys
import logging
import bb.client
from docopt import docopt
from ConfigParser import SafeConfigParser


def get_email_from_git_config():
    filename = os.path.expanduser('~/.gitconfig')
    if not os.path.exists(filename):
        return None

    git_config = SafeConfigParser()
    git_config.read(filename)

    if 'user' not in git_config.sections():
        return None

    email = None
    try:
        email = git_config.get('user', 'email')
    except:
        pass

    return email


def configure_client(username=None, password=None, email=None, config_file=bb.client.DEFAULT_CONFIG_FILE):
    if not username:
        username = os.environ.get('USER')
        if not username:
            username = raw_input('Bitbucket username: ')

    if not password:
        password = raw_input('Bitbucket password: ')

    if not email:
        email = get_email_from_git_config()
        if not email:
            email = raw_input('Atlassian ID e-mail: ')

    bb_config = SafeConfigParser()
    bb_config.read(config_file)

    if 'auth' not in bb_config.sections():
        bb_config.add_section('auth')

    bb_config.set('auth', 'username', username)
    bb_config.set('auth', 'password', password)
    bb_config.set('auth', 'email', email)

    f = open(config_file, 'w')
    bb_config.write(f)
    f.close()


def list_repos(client=None, owner=None):
    if not client:
        raise Exception('A valid client object must be passed in')

    """List all repositories"""
    repos = client.repos(owner=owner)
    for repo in sorted(repos, key=lambda r: '/'.join((r.owner, r.name,))):
        print('{name:40s} {description:40s}'.format(name='/'.join((repo.owner, repo.name,)),
                                                    description=repo.description))


def main():
    args = docopt(__doc__, version='bbcli version 0.1')
    print(args)

    if args['<command>'] is None:
        print(__doc__)
        return 1

    log_level = logging.ERROR
    if args['--debug']:
        log_level = logging.DEBUG

    logging.basicConfig(level=log_level)

    if args['configure']:
        configure_client(username=args['--user'],
                         password=args['--password'],
                         email=args['--email'],
                         config_file=args['--config'])

    else:
        client = bb.client.BitbucketClient()
        client.load_config(config_file=args['--config'])

        if args['repos']:
            list_repos(client=client, owner=args['--by-owner'])

        elif args['create'] and args['repo']:
            repo = client.create_repo(args['<repo>'],
                                      language=args['--language'],
                                      public=args['--public'])
            print(repo)

        else:
            print(__doc__)
            return 1

    return 0

if __name__ == '__main__':
    sys.exit(main())
